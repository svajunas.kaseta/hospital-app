<?php

namespace Database\Factories;

use DateInterval;
use Illuminate\Database\Eloquent\Factories\Factory;

class PrescriptionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $dateTime = $this->faker->dateTimeBetween('-6 months', '+12 months');
        $deletedOrNot = [
            null,
            $dateTime,
        ];
        return [
            'name' => $this->faker->word(),
            'quantity' => $this->faker->numberBetween(0, 1000),
            'expiration_date' => $this->faker->dateTimeBetween('-6 months', '+12 months')->format('Y-m-d'),
            'created_at' => $dateTime,
            'updated_at' => $dateTime,
            'deleted_at' => $this->faker->randomElement($deletedOrNot),
        ];
    }
}
